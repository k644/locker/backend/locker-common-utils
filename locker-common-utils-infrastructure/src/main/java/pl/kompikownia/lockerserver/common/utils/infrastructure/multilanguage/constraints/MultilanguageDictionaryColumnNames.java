package pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.constraints;

public class MultilanguageDictionaryColumnNames {
    public static final String TABLE_NAME = "translations";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_DICTIONARY_NAME = "dictionary_name";
    public static final String COLUMN_CODE = "code";
    public static final String COLUMN_LANGUAGE = "language";
    public static final String COLUMN_TRANSLATION = "translation";
}

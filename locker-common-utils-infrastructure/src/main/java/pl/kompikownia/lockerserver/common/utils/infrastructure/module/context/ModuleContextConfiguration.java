package pl.kompikownia.lockerserver.common.utils.infrastructure.module.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolderImpl;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContext;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContextFactory;

@Configuration
public class ModuleContextConfiguration {

    @Bean
    public ContextFactory<ModuleContext> moduleContextFactory() {
        return new ModuleContextFactory();
    }

    @Bean
    public ContextHolder<ModuleContext> moduleContextHolder(ContextFactory<ModuleContext> moduleContextFactory) {
        return new ContextHolderImpl<>(moduleContextFactory);
    }
}

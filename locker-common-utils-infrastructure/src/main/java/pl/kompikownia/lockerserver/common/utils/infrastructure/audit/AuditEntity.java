package pl.kompikownia.lockerserver.common.utils.infrastructure.audit;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;
import pl.kompikownia.lockerserver.common.utils.infrastructure.audit.constraints.AuditEntityColumnNames;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.time.LocalDateTime;

@Getter
@Setter
@SuperBuilder
@NoArgsConstructor
@MappedSuperclass
public class AuditEntity {

    @Column(name = AuditEntityColumnNames.AUDIT_CREATION_ID_COLUMN)
    private String creationUserId;

    @Column(name = AuditEntityColumnNames.AUDIT_CREATION_DATE_COLUMN)
    private LocalDateTime creationDate;

    @Column(name = AuditEntityColumnNames.AUDIT_MODIFICATION_ID_COLUMN)
    private String modificationUserId;

    @Column(name = AuditEntityColumnNames.AUDIT_MODIFICATION_DATE)
    private LocalDateTime modificationDate;

    @Column(name = AuditEntityColumnNames.AUDIT_VERSION_COLUMN)
    private Integer version;
}

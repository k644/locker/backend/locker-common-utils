package pl.kompikownia.lockerserver.common.utils.infrastructure.event;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.core.MessageListener;
import pl.kompikownia.lockerserver.common.utils.domain.event.EventReceiver;

import java.lang.reflect.InvocationTargetException;

@Slf4j
@RequiredArgsConstructor
public class AmqpMessageListener implements MessageListener {

    private static final String EVENT_TYPE_FIELD = "eventType";
    private static final String EVENT_BODY_FIELD = "eventBody";

    private final EventReceiver eventReceiver;
    private final ObjectMapper objectMapper;

    @Override
    public void onMessage(Message message) {
        val json = new String(message.getBody());
        try {
            val node = objectMapper.readTree(json);
            val eventType = node.get(EVENT_TYPE_FIELD);
            val eventBodyClass = Class.forName(eventType.asText());
            val eventBody = objectMapper.treeToValue(node.get(EVENT_BODY_FIELD), eventBodyClass);
            val eventReceiverMethods = eventReceiver.getClass().getMethods();
            for(val method: eventReceiverMethods) {
                val methodEventTypes = method.getParameterTypes();
                if (methodEventTypes.length == 1 ) {
                    if (methodEventTypes[0].equals(eventBody.getClass())) {
                        log.debug("Started processing event {} with args {}", eventType.asText(), eventBody.toString());
                        val currentTime = System.currentTimeMillis();
                        method.invoke(eventReceiver, eventBody);
                        log.debug("Processing event {} ended in time {} ms", eventType.asText(), System.currentTimeMillis() - currentTime);
                    }
                }

            }
        } catch (JsonProcessingException | ClassNotFoundException e) {
            log.error("Exception occured during receiving message: ", e);
        } catch (IllegalAccessException e) {
            log.error("Exception occured during access to EventReceiver: ", e);
        } catch (InvocationTargetException e) {
            log.error("Exception occured during invocation EventReceiver: ", e);
        }
    }


}

package pl.kompikownia.lockerserver.common.utils.infrastructure.audit;

import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Profile;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContext;

@RequiredArgsConstructor
@Profile("!AUDIT_DATA_RESOLVER_MOCK")
public class AuditDataResolverImpl implements AuditDataResolver {

    private final ContextHolder<ModuleContext> contextHolder;

    private static final String CAUSE_SYSTEM_USER_ID = "-1";

    @Override
    public String getUserId(Cause cause) {
        if (cause == Cause.SYSTEM) {
            return CAUSE_SYSTEM_USER_ID;
        }
        else {
            return contextHolder.getContext().getUserId();
        }
    }
}

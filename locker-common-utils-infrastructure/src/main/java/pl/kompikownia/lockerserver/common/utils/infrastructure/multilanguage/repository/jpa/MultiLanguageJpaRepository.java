package pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.repository.jpa;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.entity.MultilanguageDictionaryEntity;

import java.util.List;
import java.util.Optional;

@Repository
public interface MultiLanguageJpaRepository extends CrudRepository<MultilanguageDictionaryEntity, Integer> {
    Optional<MultilanguageDictionaryEntity> findByDictionaryNameAndCodeAndLanguage(String dictionaryName, String code, String language);

    List<MultilanguageDictionaryEntity> findBydictionaryNameAndCodeInAndLanguage(String dictionaryName, List<String> code, String language);

    List<MultilanguageDictionaryEntity> findByDictionaryNameAndCode(String dictionaryName, String code);
}

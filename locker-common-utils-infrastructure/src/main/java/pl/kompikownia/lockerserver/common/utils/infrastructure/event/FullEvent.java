package pl.kompikownia.lockerserver.common.utils.infrastructure.event;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import pl.kompikownia.lockerserver.common.utils.domain.event.Event;

@Getter
@NoArgsConstructor(access = AccessLevel.PRIVATE)
@AllArgsConstructor(staticName = "of")
public class FullEvent {
    private String eventType;
    private Object eventBody;
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.event.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.kompikownia.lockerserver.common.utils.domain.event.EventReceiver;
import pl.kompikownia.lockerserver.common.utils.infrastructure.event.AmqpMessageListener;

import java.util.ArrayList;
import java.util.List;

@Import({GeneralAMQPConfiguration.class})
@Configuration
public class AMQPReceiverConfiguration {

    @Bean
    public TopicExchange exchange() {
        return new TopicExchange("amq.topic");
    }

    @Bean
    public ObjectMapper objectMapper() {
        return new ObjectMapper();
    }

    @Bean
    public AMQPReceiverScanner amqpReceiverScanner(final TopicExchange topicExchange,
                                                   final ConnectionFactory connectionFactory,
                                                   final ObjectMapper objectMapper) {
        return new AMQPReceiverScanner(topicExchange, connectionFactory, objectMapper);
    }
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.audit;

public interface AuditDataResolver {
    String getUserId(Cause cause);
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.event.configuration;

import lombok.val;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GeneralAMQPConfiguration {
    @Bean
    public ConnectionFactory connectionFactory(
            @Value("${pl.kompikownia.lockerserver.amqp.hostname}") final String amqpHostname,
            @Value("${pl.kompikownia.lockerserver.amqp.username}") final String amqpUsername,
            @Value("${pl.kompikownia.lockerserver.amqp.password}") final String amqpPassword
    ) {
        val connectionFactory = new CachingConnectionFactory();
        connectionFactory.setAddresses(amqpHostname);
        connectionFactory.setUsername(amqpUsername);
        connectionFactory.setPassword(amqpPassword);
        return connectionFactory;
    }

}

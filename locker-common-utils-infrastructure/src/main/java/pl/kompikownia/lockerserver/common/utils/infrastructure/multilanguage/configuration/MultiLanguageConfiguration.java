package pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.configuration;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import pl.kompikownia.lockerserver.common.utils.domain.converter.Converter;
import pl.kompikownia.lockerserver.common.utils.domain.repository.MultiLanguageRepository;
import pl.kompikownia.lockerserver.common.utils.domain.types.DictionaryEntry;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.converter.MultiLanguageDictionaryEntryConverter;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.entity.MultilanguageDictionaryEntity;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.repository.MultiLanguageRepositoryImpl;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.repository.jpa.MultiLanguageJpaRepository;

@EnableJpaRepositories({
        "pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.repository.jpa"
})
@EntityScan({
        "pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.entity"
})
public class MultiLanguageConfiguration {

    @Bean
    public Converter<DictionaryEntry, MultilanguageDictionaryEntity> multilanguageDictionaryEntityConverter() {
        return new MultiLanguageDictionaryEntryConverter();
    }

    @Bean
    public MultiLanguageRepository multiLanguageRepository(
            final MultiLanguageJpaRepository multiLanguageJpaRepository,
            Converter<DictionaryEntry, MultilanguageDictionaryEntity> dictionaryEntityConverter) {
        return new MultiLanguageRepositoryImpl(multiLanguageJpaRepository, dictionaryEntityConverter);
    }
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.audit.constraints;

public class AuditEntityColumnNames {
    public static final String AUDIT_CREATION_ID_COLUMN = "audit_cu_id";
    public static final String AUDIT_CREATION_DATE_COLUMN = "audit_cu_date";
    public static final String AUDIT_MODIFICATION_ID_COLUMN = "audit_md_id";
    public static final String AUDIT_MODIFICATION_DATE = "audit_md_date";
    public static final String AUDIT_VERSION_COLUMN = "version";
}

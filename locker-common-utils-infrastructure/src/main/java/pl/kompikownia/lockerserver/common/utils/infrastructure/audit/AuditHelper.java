package pl.kompikownia.lockerserver.common.utils.infrastructure.audit;

import lombok.RequiredArgsConstructor;

import java.time.LocalDateTime;

@RequiredArgsConstructor
public class AuditHelper {

    private final AuditDataResolver auditDataResolver;

    public void pushAuditCreateInformationByCause(AuditEntity auditEntity, Cause cause) {
        auditEntity.setVersion(1);
        auditEntity.setCreationUserId(auditDataResolver.getUserId(cause));
        auditEntity.setCreationDate(LocalDateTime.now());
    }

    public void pushAuditUpdateInformationByCause(AuditEntity auditEntity, Cause cause) {
        auditEntity.setVersion(auditEntity.getVersion() + 1);
        auditEntity.setModificationUserId(auditDataResolver.getUserId(cause));
        auditEntity.setModificationDate(LocalDateTime.now());
    }
}

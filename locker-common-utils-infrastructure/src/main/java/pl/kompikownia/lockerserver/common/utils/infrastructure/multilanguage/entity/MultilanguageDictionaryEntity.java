package pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.constraints.MultilanguageDictionaryColumnNames;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Setter
@NoArgsConstructor
@Table(name = MultilanguageDictionaryColumnNames.TABLE_NAME)
@Entity
public class MultilanguageDictionaryEntity {

    @Id
    @Column(name = MultilanguageDictionaryColumnNames.COLUMN_ID)
    private Integer id;

    @Column(name = MultilanguageDictionaryColumnNames.COLUMN_DICTIONARY_NAME)
    private String dictionaryName;

    @Column(name = MultilanguageDictionaryColumnNames.COLUMN_CODE)
    private String code;

    @Column(name = MultilanguageDictionaryColumnNames.COLUMN_LANGUAGE)
    private String language;

    @Column(name = MultilanguageDictionaryColumnNames.COLUMN_TRANSLATION)
    private String translation;
}

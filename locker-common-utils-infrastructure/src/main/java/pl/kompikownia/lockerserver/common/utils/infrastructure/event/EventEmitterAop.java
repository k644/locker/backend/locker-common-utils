package pl.kompikownia.lockerserver.common.utils.infrastructure.event;

import lombok.RequiredArgsConstructor;
import lombok.val;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Value;
import pl.kompikownia.lockerserver.common.utils.domain.event.Event;
import pl.kompikownia.lockerserver.common.utils.domain.event.EventSender;

@Aspect
@RequiredArgsConstructor
public class EventEmitterAop {
    private final EventSender eventSender;

    @Value("${pl.kompikownia.lockerserver.amqp.destinationTopic}")
    private String destinationTopic;

    @Pointcut("@within(pl.kompikownia.lockerserver.common.utils.domain.event.EventEmitter)")
    public void onExecutionAnyMethodFromEventEmitterClass() {}

    @Pointcut("execution(* *(pl.kompikownia.lockerserver.common.utils.domain.event.Event+))")
    public void onExecutionEmitMethodWithProperArgument() {}

    @Pointcut("execution(* *(pl.kompikownia.lockerserver.common.utils.domain.event.Event+, String))")
    public void onExecutionEmitMethodWithDestinationTopic() {}

    @Around("onExecutionAnyMethodFromEventEmitterClass() && onExecutionEmitMethodWithProperArgument()")
    public Object sendEvent(ProceedingJoinPoint joinPoint) throws Throwable {
        val args = joinPoint.getArgs();
        val result = joinPoint.proceed(args);
        eventSender.sendEvent((Event) args[0], destinationTopic);
        return result;
    }

    @Around("onExecutionAnyMethodFromEventEmitterClass() && onExecutionEmitMethodWithDestinationTopic()")
    public Object sendEventToSpecificTopic(ProceedingJoinPoint joinPoint) throws Throwable {
        val args = joinPoint.getArgs();
        val result = joinPoint.proceed(args);
        eventSender.sendEvent((Event) args[0], (String) args[1]);
        return result;
    }
}

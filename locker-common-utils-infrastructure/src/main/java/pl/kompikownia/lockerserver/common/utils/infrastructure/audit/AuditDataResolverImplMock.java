package pl.kompikownia.lockerserver.common.utils.infrastructure.audit;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Profile("AUDIT_DATA_RESOLVER_MOCK")
public class AuditDataResolverImplMock implements AuditDataResolver {

    private static final String CAUSE_SYSTEM_USER_ID = "-1";
    private static final String CAUSE_MOCKED_USER_ID = "-10";

    @Override
    public String getUserId(Cause cause) {
        if (cause == Cause.SYSTEM) {
            return CAUSE_SYSTEM_USER_ID;
        }
        else {
            return CAUSE_MOCKED_USER_ID;
        }
    }
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.event;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.val;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import pl.kompikownia.lockerserver.common.utils.domain.event.Event;
import pl.kompikownia.lockerserver.common.utils.domain.event.EventSender;

@RequiredArgsConstructor
public class RabbitMqEventSenderImpl implements EventSender {
    private final RabbitTemplate rabbitTemplate;

    @Override
    public void sendEvent(Event event, String destinationTopic) {
        val fullEvent = FullEvent.of(event.getClass().getName(), event);
        val eventAsString = convertObjectToJson(fullEvent);
        rabbitTemplate.convertAndSend("amq.topic", destinationTopic, eventAsString);
    }

    private String convertObjectToJson(FullEvent event) {
        val objectMapper = new ObjectMapper();
        try {
            return objectMapper.writeValueAsString(event);
        }
        catch(JsonProcessingException ex) {
            throw new RuntimeException(ex);
        }
    }
}

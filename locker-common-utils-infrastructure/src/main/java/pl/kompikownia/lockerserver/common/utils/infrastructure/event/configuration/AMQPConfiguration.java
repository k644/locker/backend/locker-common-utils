package pl.kompikownia.lockerserver.common.utils.infrastructure.event.configuration;

import lombok.val;
import org.springframework.amqp.rabbit.connection.CachingConnectionFactory;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import pl.kompikownia.lockerserver.common.utils.domain.event.EventSender;
import pl.kompikownia.lockerserver.common.utils.infrastructure.event.EventEmitterAop;
import pl.kompikownia.lockerserver.common.utils.infrastructure.event.RabbitMqEventSenderImpl;

@Configuration
@Import({GeneralAMQPConfiguration.class})
public class AMQPConfiguration {

    @Bean
    public RabbitTemplate rabbitTemplate(final ConnectionFactory connectionFactory) {
        return new RabbitTemplate(connectionFactory);
    }

    @Bean
    public EventSender eventSender(final RabbitTemplate rabbitTemplate) {
        return new RabbitMqEventSenderImpl(rabbitTemplate);
    }

    @Bean
    public EventEmitterAop eventEmitterAop(final EventSender eventSender) {
        return new EventEmitterAop(eventSender);
    }
}

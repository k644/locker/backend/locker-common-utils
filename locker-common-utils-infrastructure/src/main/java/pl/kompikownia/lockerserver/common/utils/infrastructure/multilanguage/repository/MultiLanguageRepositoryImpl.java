package pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.repository;

import lombok.RequiredArgsConstructor;
import pl.kompikownia.lockerserver.common.utils.domain.converter.Converter;
import pl.kompikownia.lockerserver.common.utils.domain.repository.MultiLanguageRepository;
import pl.kompikownia.lockerserver.common.utils.domain.types.DictionaryEntry;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.entity.MultilanguageDictionaryEntity;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.repository.jpa.MultiLanguageJpaRepository;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@RequiredArgsConstructor
public class MultiLanguageRepositoryImpl implements MultiLanguageRepository {

    private final MultiLanguageJpaRepository multiLanguageJpaRepository;

    private final Converter<DictionaryEntry, MultilanguageDictionaryEntity> dictionaryEntityConverter;

    @Override
    public Optional<DictionaryEntry> getTranslationForCode(String dictionaryName, String code, Locale language) {
        return multiLanguageJpaRepository.findByDictionaryNameAndCodeAndLanguage(dictionaryName, code, language.getLanguage())
                .map(dictionaryEntityConverter::convert);
    }

    @Override
    public Map<String, String> getTranslationsForCodeList(String dictionaryName, List<String> codes, String language) {
        return multiLanguageJpaRepository.findBydictionaryNameAndCodeInAndLanguage(dictionaryName, codes, language)
                .stream()
                .map(dictionaryEntityConverter::convert)
                .collect(Collectors.toMap(DictionaryEntry::getCode, DictionaryEntry::getTranslation));
    }

    @Override
    public List<DictionaryEntry> getAllTranslationsForCode(String dictionaryName, String code) {
        return multiLanguageJpaRepository.findByDictionaryNameAndCode(dictionaryName, code)
                .stream()
                .map(dictionaryEntityConverter::convert)
                .collect(Collectors.toList());
    }
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.external.module.context;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolderImpl;
import pl.kompikownia.lockerserver.common.utils.domain.context.external.module.ExternalModuleContext;
import pl.kompikownia.lockerserver.common.utils.domain.context.external.module.ExternalModuleContextFactory;

@Configuration
public class ExternalModuleContextConfiguration {

    @Bean
    public ContextFactory<ExternalModuleContext> externalModuleContextFactory() {
        return new ExternalModuleContextFactory();
    }

    @Bean
    public ContextHolder<ExternalModuleContext> externalModuleContextHolder(final ContextFactory<ExternalModuleContext> externalModuleContextFactory) {
        return new ContextHolderImpl<>(externalModuleContextFactory);
    }
}

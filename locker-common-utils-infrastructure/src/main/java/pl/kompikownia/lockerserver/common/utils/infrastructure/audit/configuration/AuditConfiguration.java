package pl.kompikownia.lockerserver.common.utils.infrastructure.audit.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import pl.kompikownia.lockerserver.common.utils.domain.context.ContextHolder;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContext;
import pl.kompikownia.lockerserver.common.utils.infrastructure.audit.AuditDataResolver;
import pl.kompikownia.lockerserver.common.utils.infrastructure.audit.AuditDataResolverImpl;
import pl.kompikownia.lockerserver.common.utils.infrastructure.audit.AuditDataResolverImplMock;
import pl.kompikownia.lockerserver.common.utils.infrastructure.audit.AuditHelper;

@Configuration
public class AuditConfiguration {

    @Bean
    @Profile("!AUDIT_DATA_RESOLVER_MOCK")
    public AuditDataResolver auditDataResolver(final ContextHolder<ModuleContext> contextHolder) {
        return new AuditDataResolverImpl(contextHolder);
    }


    @Bean
    @Profile("AUDIT_DATA_RESOLVER_MOCK")
    public AuditDataResolver auditDataResolverMock() {
        return new AuditDataResolverImplMock();
    }

    @Bean
    public AuditHelper auditHelper(AuditDataResolver auditDataResolver) {
        return new AuditHelper(auditDataResolver);
    }
}

package pl.kompikownia.lockerserver.common.utils.infrastructure.event.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import lombok.val;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.rabbit.listener.SimpleMessageListenerContainer;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import pl.kompikownia.lockerserver.common.utils.domain.event.EventReceiver;
import pl.kompikownia.lockerserver.common.utils.infrastructure.event.AmqpMessageListener;

@Slf4j
@RequiredArgsConstructor
public class AMQPReceiverScanner implements ApplicationContextAware {

    private final TopicExchange topicExchange;
    private final ConnectionFactory connectionFactory;
    private final ObjectMapper objectMapper;

    private void prepareAndRegisterEventReceiver(EventReceiver eventReceiver,
                                                 ApplicationContext applicationContext) {
        val queue = createQueue(eventReceiver.getClass().getSimpleName());
        val binding = BindingBuilder.bind(queue)
                .to(topicExchange)
                .with(eventReceiver.getRoutingKey());
        val messageListenerAdapter = new AmqpMessageListener(eventReceiver, objectMapper);
        val container = new SimpleMessageListenerContainer();
        container.setConnectionFactory(connectionFactory);
        container.setQueueNames(eventReceiver.getClass().getSimpleName());
        container.setMessageListener(messageListenerAdapter);
        ConfigurableListableBeanFactory beanFactory = ((ConfigurableApplicationContext) applicationContext).getBeanFactory();
        beanFactory.registerSingleton("queue_"+eventReceiver.getClass().getSimpleName(), queue);
        beanFactory.registerSingleton("binding_"+eventReceiver.getClass().getSimpleName(), binding);
        beanFactory.registerSingleton("container_"+eventReceiver.getClass().getSimpleName(), container);
    }

    private Queue createQueue(String queueName) {
        return new Queue(queueName, false);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        val eventReceivers = applicationContext.getBeansOfType(EventReceiver.class);
        for (val eventReceiver: eventReceivers.values()) {
            prepareAndRegisterEventReceiver(eventReceiver, applicationContext);
        }
    }
}

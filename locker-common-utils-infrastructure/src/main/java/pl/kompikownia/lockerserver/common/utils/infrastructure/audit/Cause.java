package pl.kompikownia.lockerserver.common.utils.infrastructure.audit;

public enum Cause {
    SYSTEM,
    USER
}

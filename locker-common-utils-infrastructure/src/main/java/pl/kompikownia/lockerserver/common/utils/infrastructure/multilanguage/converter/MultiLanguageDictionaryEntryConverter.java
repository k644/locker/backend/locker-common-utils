package pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.converter;

import pl.kompikownia.lockerserver.common.utils.domain.converter.Converter;
import pl.kompikownia.lockerserver.common.utils.domain.types.DictionaryEntry;
import pl.kompikownia.lockerserver.common.utils.infrastructure.multilanguage.entity.MultilanguageDictionaryEntity;

import java.util.Locale;

public class MultiLanguageDictionaryEntryConverter implements Converter<DictionaryEntry, MultilanguageDictionaryEntity> {

    @Override
    public DictionaryEntry convert(MultilanguageDictionaryEntity multilanguageDictionaryEntity) {
        return DictionaryEntry
                .builder()
                .code(multilanguageDictionaryEntity.getCode())
                .language(Locale.forLanguageTag(multilanguageDictionaryEntity.getLanguage()))
                .translation(multilanguageDictionaryEntity.getTranslation())
                .build();
    }
}

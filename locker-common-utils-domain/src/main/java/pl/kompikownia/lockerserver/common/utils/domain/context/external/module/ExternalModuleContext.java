package pl.kompikownia.lockerserver.common.utils.domain.context.external.module;

import lombok.*;
import pl.kompikownia.lockerserver.common.utils.domain.context.Context;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class ExternalModuleContext implements Context {
    private String serialNumber;
    private String assignedLockerId;
}

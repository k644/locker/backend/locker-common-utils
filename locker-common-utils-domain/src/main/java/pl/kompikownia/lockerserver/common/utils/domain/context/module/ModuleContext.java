package pl.kompikownia.lockerserver.common.utils.domain.context.module;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import pl.kompikownia.lockerserver.common.utils.domain.context.UserContext;
import pl.kompikownia.lockerserver.common.utils.domain.types.Language;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@ToString
public class ModuleContext implements UserContext {
    private String userId;
    private String tokenId;
    private Language language;
    private List<String> permissionList;
}

package pl.kompikownia.lockerserver.common.utils.domain.exception;

public class ModuleNotFoundException extends BusinessException {
    public ModuleNotFoundException(String message) {
        super(String.format("Module with id %s cannot be found", message));
    }

    public ModuleNotFoundException() {
        super("Module was not found");
    }
}

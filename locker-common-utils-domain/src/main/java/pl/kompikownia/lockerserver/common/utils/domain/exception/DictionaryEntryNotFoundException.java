package pl.kompikownia.lockerserver.common.utils.domain.exception;

import lombok.Getter;

import java.util.Locale;

@Getter
public class DictionaryEntryNotFoundException extends BusinessException {
    private final String dictionaryName;
    private final String dictionaryCode;
    private final Locale language;

    public DictionaryEntryNotFoundException(String dictionaryName, String dictionaryCode, Locale language) {
        super(String.format("Translation for %s not found for %s in dictionary %s", dictionaryCode, language, dictionaryName));
        this.dictionaryName = dictionaryName;
        this.dictionaryCode = dictionaryCode;
        this.language = language;
    }
}

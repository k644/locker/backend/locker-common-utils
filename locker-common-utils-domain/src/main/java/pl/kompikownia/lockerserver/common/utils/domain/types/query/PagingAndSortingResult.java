package pl.kompikownia.lockerserver.common.utils.domain.types.query;

import lombok.*;

import java.util.List;

@Getter
@Builder
@ToString
@AllArgsConstructor(access = AccessLevel.PRIVATE)
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PagingAndSortingResult<T> {
    private Long elementsCount;
    private Long allElementsCount;
    private Integer actualPage;
    private List<T> element;
}

package pl.kompikownia.lockerserver.common.utils.domain.exception;

import lombok.AllArgsConstructor;
import lombok.Getter;
import pl.kompikownia.lockerserver.common.utils.domain.validation.ValidationError;

@Getter
@AllArgsConstructor(staticName = "of")
public class BusinessValidationException extends RuntimeException {
    ValidationError validationError;
}

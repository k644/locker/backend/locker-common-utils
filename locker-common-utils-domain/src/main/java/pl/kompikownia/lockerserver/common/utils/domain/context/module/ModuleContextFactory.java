package pl.kompikownia.lockerserver.common.utils.domain.context.module;

import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;

public class ModuleContextFactory extends ContextFactory<ModuleContext> {

    @Override
    public ModuleContext buildEmptyContext() {
        return new ModuleContext();
    }
}

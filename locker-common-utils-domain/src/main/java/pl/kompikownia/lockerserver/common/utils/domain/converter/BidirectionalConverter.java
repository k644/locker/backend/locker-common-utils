package pl.kompikownia.lockerserver.common.utils.domain.converter;

public interface BidirectionalConverter<Dest, Source> extends Converter<Dest, Source> {

    Source convertReverse(Dest dest);
}

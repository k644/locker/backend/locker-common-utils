package pl.kompikownia.lockerserver.common.utils.domain.types.query;

public enum SortDirection {
    ASC,
    DESC
}

package pl.kompikownia.lockerserver.common.utils.domain.locker.module;

import java.util.Optional;

public interface LockerModuleContextDataProvider {
    Optional<LockerModuleData> findLockerModuleBySerialNumber(String serialNumber);
}

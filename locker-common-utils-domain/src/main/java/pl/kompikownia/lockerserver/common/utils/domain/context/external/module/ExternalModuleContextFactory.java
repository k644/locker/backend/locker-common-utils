package pl.kompikownia.lockerserver.common.utils.domain.context.external.module;

import pl.kompikownia.lockerserver.common.utils.domain.context.ContextFactory;

public class ExternalModuleContextFactory extends ContextFactory<ExternalModuleContext> {

    @Override
    public ExternalModuleContext buildEmptyContext() {
        return new ExternalModuleContext();
    }
}

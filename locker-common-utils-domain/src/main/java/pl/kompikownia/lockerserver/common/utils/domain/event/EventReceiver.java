package pl.kompikownia.lockerserver.common.utils.domain.event;

public interface EventReceiver {

    String getRoutingKey();
}

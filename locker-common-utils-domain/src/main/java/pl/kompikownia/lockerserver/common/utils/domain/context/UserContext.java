package pl.kompikownia.lockerserver.common.utils.domain.context;

import pl.kompikownia.lockerserver.common.utils.domain.types.Language;

import java.util.List;

public interface UserContext extends Context {
    String getUserId();
    String getTokenId();
    List<String> getPermissionList();
    Language getLanguage();
}

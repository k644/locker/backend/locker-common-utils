package pl.kompikownia.lockerserver.common.utils.domain.context;

public interface ContextHolder<T extends Context> {
    void clearContext();
    T getContext();
    void setContext(T context);
}

package pl.kompikownia.lockerserver.common.utils.domain.validation;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class ValidationError {
    private String fieldName;
    private ValidationErrorEnum validationErrorEnum;
}

package pl.kompikownia.lockerserver.common.utils.domain.context;

import lombok.val;
import pl.kompikownia.lockerserver.common.utils.domain.context.module.ModuleContext;


public class ContextHolderImpl<T extends Context> implements ContextHolder<T>{
    private final ThreadLocal<T> holder = new ThreadLocal<>();
    private final ContextFactory<T> contextFactory;

    public ContextHolderImpl(ContextFactory<T> contextFactory) {
        this.contextFactory = contextFactory;
    }

    public void clearContext() {
        holder.remove();
    }

    public T getContext() {
        T ctx = holder.get();
        if (ctx == null) {
            ctx = contextFactory.buildEmptyContext();
            holder.set(ctx);
        }
        return ctx;
    }

    public void setContext(T context) {
        holder.set(context);
    }
}

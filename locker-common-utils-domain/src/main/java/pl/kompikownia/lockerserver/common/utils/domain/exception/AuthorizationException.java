package pl.kompikownia.lockerserver.common.utils.domain.exception;

public class AuthorizationException extends SystemException {
    public AuthorizationException(String message) {
        super(message);
    }
}

package pl.kompikownia.lockerserver.common.utils.domain.event;

public interface EventSender {
    void sendEvent(Event event, String destinationTopic);
}

package pl.kompikownia.lockerserver.common.utils.domain.converter;

public interface Converter<Dest, Source> {
    Dest convert(Source source);
}

package pl.kompikownia.lockerserver.common.utils.domain.context;

public abstract class ContextFactory<T extends Context> {

    public abstract T buildEmptyContext();
}

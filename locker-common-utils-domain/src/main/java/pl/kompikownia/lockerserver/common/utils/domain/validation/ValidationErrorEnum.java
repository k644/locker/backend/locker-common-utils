package pl.kompikownia.lockerserver.common.utils.domain.validation;

public enum ValidationErrorEnum {
    DUPLICATED_KEY,
    BAD_FORMAT,
    NOT_EMPTY,
    UNKNOWN
}

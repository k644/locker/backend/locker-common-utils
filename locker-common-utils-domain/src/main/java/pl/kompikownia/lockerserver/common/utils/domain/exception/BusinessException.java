package pl.kompikownia.lockerserver.common.utils.domain.exception;

public class BusinessException extends SystemException {

    public BusinessException() {
    }

    public BusinessException(String message) {
        super(message);
    }
}

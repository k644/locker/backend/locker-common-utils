package pl.kompikownia.lockerserver.common.utils.domain.types;

public enum Language {
    POLISH, ENGLISH
}

package pl.kompikownia.lockerserver.common.utils.domain.types;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

import java.util.Locale;

@Builder
@Getter
@ToString
public class DictionaryEntry {
    private String code;
    private Locale language;
    private String translation;
}

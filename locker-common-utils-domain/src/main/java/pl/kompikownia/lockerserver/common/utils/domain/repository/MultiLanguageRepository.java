package pl.kompikownia.lockerserver.common.utils.domain.repository;

import pl.kompikownia.lockerserver.common.utils.domain.types.DictionaryEntry;

import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Optional;

public interface MultiLanguageRepository {
    Optional<DictionaryEntry> getTranslationForCode(String dictionaryName, String code, Locale language);

    Map<String, String> getTranslationsForCodeList(String dictionaryName, List<String> codes, String language);

    List<DictionaryEntry> getAllTranslationsForCode(String dictionaryName, String code);
}

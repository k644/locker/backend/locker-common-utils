package pl.kompikownia.lockerserver.common.utils.domain.locker.module;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

@Getter
@AllArgsConstructor(staticName = "of")
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class LockerModuleData {
    private String ownerLockerId;
    private String serialNumber;
}

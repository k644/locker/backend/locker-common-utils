package pl.kompikownia.lockerserver.common.utils.domain;

import lombok.val;

import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.stream.Collectors;

public class ObjectPatcher {

    public static<T> void patchObject(T objectToPatch, T reference) {
        val fields = objectToPatch.getClass().getDeclaredFields();
        val fieldsToUpdate = Arrays.stream(fields)
                .filter(field -> isFieldNull(field, objectToPatch))
                .collect(Collectors.toList());
        fieldsToUpdate.forEach(field -> {
            updateField(field, objectToPatch, reference);
        });
    }

    private static boolean isFieldNull(Field field, Object objectToPatch) {
        try {
            field.setAccessible(true);
            boolean result = field.get(objectToPatch) != null;
            field.setAccessible(false);
            return result;
        } catch (IllegalAccessException e) {
            field.setAccessible(false);
            throw new RuntimeException(e);
        }
    }

    private static void updateField(Field field, Object objectToPatch, Object referenceObject) {
        try {
            field.setAccessible(true);
            field.set(referenceObject, field.get(objectToPatch));
            field.setAccessible(false);
        }
        catch(IllegalAccessException ex) {
            field.setAccessible(false);
            throw new RuntimeException(ex);
        }
    }
}

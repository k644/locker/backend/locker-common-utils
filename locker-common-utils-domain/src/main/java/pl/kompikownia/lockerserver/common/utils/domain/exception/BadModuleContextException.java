package pl.kompikownia.lockerserver.common.utils.domain.exception;

public class BadModuleContextException extends BusinessException {
    public BadModuleContextException(String parameter) {
        super(String.format("Bad module context was provided (parameter: %s)", parameter));
    }
}

package pl.kompikownia.lockerserver.common.utils.domain.types.query;

import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@Getter
@Builder
@ToString
public class SearchResult<T> {
    private Long elementsCount;
    private T element;
}

package pl.kompikownia.lockerserver.common.utils.domain.types.query;

import lombok.*;

@Getter
@AllArgsConstructor(staticName = "of")
@ToString
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class SortField {
    private String fieldName;
    private SortDirection sortDirection;
}

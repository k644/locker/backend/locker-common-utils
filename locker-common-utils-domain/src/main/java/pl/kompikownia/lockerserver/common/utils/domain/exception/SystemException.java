package pl.kompikownia.lockerserver.common.utils.domain.exception;

public class SystemException extends RuntimeException {
    public SystemException(String message) {
        super(message);
    }

    public SystemException() {
    }
}
